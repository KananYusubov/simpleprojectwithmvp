package com.example.kananyusub.simpleprojectwithmvp.view;

import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.kananyusub.simpleprojectwithmvp.R;
import com.example.kananyusub.simpleprojectwithmvp.presenter.MainActivityPresenter;

public class MainActivity extends AppCompatActivity
        implements MainActivityPresenter.View {
    private TextView mTextViewFullName, mTextViewEmail;
    private MainActivityPresenter mMainActivityPresenter;
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mMainActivityPresenter = new MainActivityPresenter(this);

        mTextViewFullName = findViewById(R.id.textView_main_fullname);
        mTextViewEmail = findViewById(R.id.textView_main_email);
        EditText mEditTextFullName = findViewById(R.id.editText_main_fullname);
        EditText mEditTextEmail = findViewById(R.id.editText_main_email);

        initProgressBar();

        mEditTextFullName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mMainActivityPresenter.updateFullName(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) hideProgressBar();
            }
        });

        mEditTextEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mMainActivityPresenter.updateEmail(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) hideProgressBar();
            }
        });
    }

    @Override
    public void updateUserNameTextView(String fullName) {
        mTextViewFullName.setVisibility(View.VISIBLE);
        mTextViewFullName.setText(fullName);
    }

    @Override
    public void updateEmailTextView(String email) {
        mTextViewEmail.setVisibility(View.VISIBLE);
        mTextViewEmail.setText(email);
    }

    @Override
    public void showProgressBar() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        mProgressBar.setVisibility(View.INVISIBLE);
    }

    private void initProgressBar() {
        mProgressBar = new ProgressBar(this, null, android.R.attr.progressBarStyleSmall);
        mProgressBar.setIndeterminate(true);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(Resources.getSystem().getDisplayMetrics().widthPixels,
                250);
        params.addRule(RelativeLayout.CENTER_IN_PARENT);
        this.addContentView(mProgressBar, params);
        showProgressBar();
    }
}
