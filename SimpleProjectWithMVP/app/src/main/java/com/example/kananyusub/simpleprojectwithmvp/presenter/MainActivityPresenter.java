package com.example.kananyusub.simpleprojectwithmvp.presenter;
import android.view.View;
import com.example.kananyusub.simpleprojectwithmvp.model.User;

public class MainActivityPresenter {
    private User mUser;
    private View mView;

    public MainActivityPresenter(View view) {
        this.mUser = new User();
        this.mView = view;
    }

    public void updateFullName(String fullName) {
        mUser.setFullname(fullName);
        mView.updateUserNameTextView(mUser.getFullname());
    }

    public void updateEmail(String email) {
        mUser.setEmail(email);
        mView.updateEmailTextView(mUser.getEmail());
    }

    public interface View {
        void updateUserNameTextView(String fullName);

        void updateEmailTextView(String email);

        void showProgressBar();

        void hideProgressBar();
    }
}
