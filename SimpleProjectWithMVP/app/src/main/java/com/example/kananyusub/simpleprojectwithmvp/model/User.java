package com.example.kananyusub.simpleprojectwithmvp.model;

public class User {
    private String mFullname = "", mEmail = "";

    public User() {
    }

    public User(String fullname, String email) {
        this.mFullname = fullname;
        this.mEmail = email;
    }

    public String getFullname() {
        return mFullname;
    }

    public void setFullname(String fullname) {
        this.mFullname = fullname;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        this.mEmail = email;
    }

    @Override
    public String toString() {
        return "Email : " + mEmail + "\nFullName : " + mFullname;
    }
}
